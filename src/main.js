import Vue from "vue";
import round from "vue-round-filter";
import App from "./App.vue";
import "bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";

Vue.config.productionTip = false;

Vue.filter("round", round);

new Vue({
  render: h => h(App)
}).$mount("#app");
