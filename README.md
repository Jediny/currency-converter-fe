# Currency converter frontend

Application goal is to make a simple currency conversion web. User will input amount, currency and destination currency. App will show the converted amount + other statistics.

## Usage

```bash
# Compiles and hot-reloads for development
$ npm run serve

# Compiles and minifies for production
$ npm run build

# Lints and fixes files
$ npm run lint
```
